package id.opusb.training.callout;

import java.util.List;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MBPartner;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;

public class CalloutOrder implements IColumnCallout {
	

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue) {
		
		if (mField.getColumnName().equals("C_BPartner_ID")) {
			bpartner(ctx, WindowNo, mTab, mField, value, oldValue);
		} else if (mField.getColumnName().equals("Description")) {
			
		}
		return null;
	}
	
	public String bpartner(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue) {
		Integer bpID = (Integer) value;
		if (bpID != null) {
			MBPartner bp = new MBPartner(ctx, bpID, null);
			StringBuilder description = new StringBuilder();
			description.append("BP Name: " + bp.getName() + "\n");
			
//			bp.getC_BP_Group().getName();
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT bpg.Name FROM C_BPartner bp ");
			sql.append("INNER JOIN C_BP_Group bpg ON (bpg.C_BP_Group_ID = bp.C_BP_Group_ID) ");
			sql.append("WHERE bp.C_BPartner_ID = ? ");
			String bpGroupName = DB.getSQLValueString(null, sql.toString(), bpID);
			description.append("BP Group: " + bpGroupName);
			
			List<MBPartner> bpList = new Query(ctx, MBPartner.Table_Name, "C_BP_Group_ID = ?", null)
			.setParameters(1000039)
			.list();
			for(MBPartner o : bpList) {
				System.out.println(o.getName());
			}
			
			int userID = Env.getContextAsInt(ctx, WindowNo, "#AD_User_ID");
			System.out.println(userID);
		}
		return "";
	}

}
