package id.opusb.training.factory;

import org.adempiere.base.IColumnCallout;
import org.adempiere.base.IColumnCalloutFactory;
import org.compiere.model.MOrder;

import id.opusb.training.callout.CalloutOrder;

public class OpusbCalloutFactory implements IColumnCalloutFactory {

	@Override
	public IColumnCallout[] getColumnCallouts(String tableName, String columnName) {
		if (tableName.equals(MOrder.Table_Name)) {
			return new IColumnCallout[]{new CalloutOrder()};
		}
		return null;
	}

}
