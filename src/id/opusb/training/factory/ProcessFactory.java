package id.opusb.training.factory;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;

import id.opusb.training.process.TestProcess;

public class ProcessFactory implements IProcessFactory {

	@Override
	public ProcessCall newProcessInstance(String className) {
		if (className.equals("id.opusb.training.process.TestProcess")) {
			return new TestProcess();
		}
		return null;
	}

}
