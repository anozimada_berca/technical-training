package id.opusb.training.factory;

import org.adempiere.base.event.AbstractEventHandler;
import org.adempiere.base.event.IEventTopics;
import org.compiere.model.MBPartner;
import org.compiere.model.MInvoice;
import org.compiere.model.PO;
import org.osgi.service.event.Event;

import id.opusb.training.event.BPartnerEvent;
import id.opusb.training.event.InvoiceEvent;

public class OpusbEventFactory extends AbstractEventHandler {

	@Override
	protected void doHandleEvent(Event event) {
		String type = event.getTopic();
		PO po = getPO(event);
		if (po != null) {
			if(po.get_TableName().equals(MBPartner.Table_Name) && type.equals(IEventTopics.PO_BEFORE_CHANGE)) {
				MBPartner bp = (MBPartner) po;
				BPartnerEvent.validateBP(bp);
			} else if(po.get_TableName().equals(MInvoice.Table_Name) && type.equals(IEventTopics.DOC_BEFORE_PREPARE)) {
				MInvoice invoice = (MInvoice) po;
				InvoiceEvent.validate(invoice);
			}
		}
	}

	@Override
	protected void initialize() {
		registerTableEvent(IEventTopics.PO_BEFORE_CHANGE, MBPartner.Table_Name);
		registerTableEvent(IEventTopics.DOC_BEFORE_PREPARE, MInvoice.Table_Name);
	}

}
