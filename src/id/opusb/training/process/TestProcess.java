package id.opusb.training.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;

public class TestProcess extends SvrProcess {

	int p_BpID = 0;
	Timestamp p_DateDocStart = null;
	Timestamp p_DateDocEnd = null;
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("C_BPartner_ID"))
				p_BpID = para[i].getParameterAsInt();
			else if (name.equals("DateDoc")) {
				p_DateDocStart = para[i].getParameterAsTimestamp();
				p_DateDocEnd = para[i].getParameter_ToAsTimestamp();
			}
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		System.out.println("C_BPartner_ID= " + p_BpID);
		System.out.println("DateDoc Start= " + p_DateDocStart);
		System.out.println("DateDoc End= " + p_DateDocEnd);
		return "";
	}

}
